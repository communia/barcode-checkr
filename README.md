# Barcode-checkr
UI to check the barcode from json/csv. Developed to fulfill the needs of a Drupal commerce site with a drupal commerce tickets ( https://www.drupal.org/project/commerce_ticketing), but it can be used with any csv list of products with a barcode.

Barcode Checkr is a webapp (html + js) to validate via barcode scanner against a list of tickets/products. It will run with a web browser. Optionally it can be run with electron app.


# Using Barcode-checkr

* Obtain the aplication in one of these flavors:
  * Standard build:
    * can be placed anywhere in the wrapper drupal module commerce_ticket_barcode_checker .
    * Open the url : https://drupal.example.com/modules/custom/commerce_ticket_barcode_checker/assets/barcode-checkr-main/dist/
  * As standalone app: 
    * Download application from the main-electron branch tags in https://gitlab.com/communia/barcode-checkr/-/releases . The electron app will run as exe in win, snap, appimage or standalone executable in gnu/linux,or dmg in mac.
* Connect the usb barcode scanner, and check that it scans codes and that it gets echoed in the "Id de ticket" box.
* CSV/JSON: Load the csv file through the inidicated box. By default CSV file must contain at least the header fields ***"Barcode"***,***"Used***, and ***"Ticket ID"***.
* Export and recover: In case that browser or tab is closed we can recover last ticket/product list. In case that you want to change computer or you want to restart the browser the safest way will be to export csv file and reload again once you start browser again.

Quick run
=======
Download latest release from https://github.com/aleixq/barcode-checkr/releases/latest , and open in modern browser. 
Additionally, you can select the example.csv found at  directory example/ to view the workflow: https://github.com/aleixq/barcode-checkr/raw/master/example/example.csv . 
(As shown in example CSV file must contain at least the header fields ***"Barcode"***,***"Used***, and ***"Ticket ID"***.)


develop
=======

This application uses vue3, vuestic ui kit. 

To develop and build vite, also electron-vite to provide the electron app. 
Dedicated branches for browser (main) and electron (electron-main)  will provide the artifacts and releases.

After downloading the repo...

To build the standard: 

```
yarn install
yarn build
```

To build the drupal asset:

`yarn build:drupal`

to build the electron app:

`yarn electron:build`

run with

`yarn electron:dev`

to live developing:

`yarn dev`


## Templates used:

### Vue 3 + Vite

This template should help get you started developing with Vue 3 in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

#### Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

### Electron-vite-vue

Inspired in this boilerplate to build the electron app: 

- https://github.com/electron-vite/electron-vite-vue
