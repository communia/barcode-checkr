# barcode-checkr
Barcode Checkr és un programa en html i javascript per verificar mitjançant un lector de codis de barres un llistat de entrades. S'utilitza mitjançant un navegador web.

* Obté el programa segons on el vulguis executar:
  * Per executar en un drupal:
    * Hauria de ser obtingut des del mòdul commerce_ticket_barcode_checker
  * Per utilitzar com a aplicació independent:
    * Baixa aplicació de https://drupal.example.com/modules/custom/commerce_ticket_barcode_checker/assets/barcode-checkr-main/dist/ en qualsevol versió de la branca electron-mainPodràs executar-la per iwndows en exe, snap i appimage o amb binari independent amb linux, o dmg amb mac.
* Connectar lector: Connectar el lector de codi de barres per usb i assegurar-se que llegeix codis i n'imprimeix el resultat a la caixa "Id de ticket".
* Entrades en json/ .csv: Cal carregar el llistat d'entrades mitjançant el botó de navegació d'arxius sota el títol "Select file Change" i seleccionar l'arxiu ".csv" o ".json" descarregat. Per defecte el fitxer JSON/CSV haurà de contenir com a mínim els camps de capçalera ***"Barcode"***,***"Used"***, i ***"Ticket ID"***.
* Exportar i recuperar: En cas de tancament accidental de la pestanya del navegador podem provar d'utilitzar el botó "recover". En cas que vulguem recarregar el navegador o reiniciar l'ordinador per continuar després en el mateix estat que teníem el llistat, recomanem utilitzar l'opció "exportar" i  recarregar posteriorment l'arxiu generat amb "Select file Change".


més informació a (Readme.md)[Readme.md]
