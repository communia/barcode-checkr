import { createI18n } from 'vue-i18n'
import { useI18nConfig } from "vuestic-ui";


const messages = {
  en: {
    language: 'English',
    vuestic: {
      search: 'Search'
    },
    // BarcodeChecker
    search: 'search',
    ticketsNotLoaded: 'tickets not loaded',
    ticketNotValid: 'ticket #{ticket_barcode} not valid', 
    NA: 'n/a',
    unableToReadFile: 'Could not load {filename}',        
    checkingAsUsedTicketWithBarcode: "Checking ticket with barcode {barcode} as used",
    ticketOk: 'Ticket #{ticket_id} OK',
    ticketFoundUsed: "ticket #{ticket_id} found but is used",
    by: 'by',
    uploadCsv: 'Upload csv/json',
    downloadCsv: 'Download csv',
    downloadJson: 'Download json',
    recover: 'Recover autosaved',
    showMenu: 'Show menu',
    hideMenu: 'Hide menu',
    selectFile: 'Select file',
    barcodeInput: 'Barcode Input',
    scanResult: 'Scan Result',
    madeWith: 'Made with',
    // fileInfo
    fileProperties: 'File Properties',
    name: 'name:',
    type: 'type:',
    size: 'size:',
    modificatonDate: 'mod date:'
  },
  ca: {
    language: 'Catala',
    vuestic: {
      search: 'Cerca'
    },
    search: 'cerca',
    ticketsNotLoaded: 'tickets no carregats',
    ticketNotValid: 'ticket #{ticket_barcode} no vàlid', 
    NA: 'n/a',
    unableToReadFile: 'No ha estat possible llegir {filename}',
    checkingAsUsedTicketWithBarcode: "Comprovant ticket amb codi {barcode} com a usat",
    ticketOk: 'Ticket #{ticket_id} vàlid',
    ticketFoundUsed: "ticket #{ticket_id} trobat però ja usat",
    by: 'per',
    uploadCsv: 'Carregar csv/json',
    downloadCsv: 'Descarregar csv',
    downloadJson: 'Descarregar json',
    recover: 'Recuperar autodesat',
    showMenu: 'Mostra menú',
    hideMenu: 'Amaga menú',
    selectFile: 'Selecciona fitxer',
    barcodeInput: 'Entrada de codi de barres',
    scanResult: 'Resultat d\'escaneig',
    madeWith: 'Fet amb',
    // fileInfo
    fileProperties: 'Propietats del fitxer',
    name: 'nom:',
    type: 'tipus:',
    size: 'mida:',
    modificatonDate: 'data de modificació:'    
  },  
};

export default createI18n({
  legacy: false,
  locale: 'ca',
  fallbackLocale: 'en',
  messages,
})