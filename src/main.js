import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import { createVuestic } from "vuestic-ui";
import i18n from './i18n'
import config from '../vuestic.config.js'
import "vuestic-ui/css";

createApp(App).use(createVuestic({ config })).use(i18n).mount('#app')